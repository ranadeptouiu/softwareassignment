/*
		Created by:Rana Depto

		Email:mail@ranadepto.com
	
	    Date:6/3/18,Time:4:24PM 
*/

package com.ranadepto.softwareassignment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class TodoController
{

	@Autowired
	TodoRepository todoRepository;

	// Get All todos
	@GetMapping("/todo")
	public List<TodoModel> getAllTodos()
	{
		return todoRepository.findAll();
	}

	// Get a Single todo
	@GetMapping("/todo/{id}")
	public TodoModel getTodoById(@PathVariable(value = "id") long todoId)
	{
		return todoRepository.findById(todoId)
				.orElseThrow(() -> new ResourceNotFoundException("Todo", "id", todoId));
	}

	// Create a new todo
	@PostMapping("/todo")
	public TodoModel createTodo(@Valid @RequestBody TodoModel todoModel)
	{
		return todoRepository.save(todoModel);
	}

	// Update a todo
	@PutMapping("/todo/{id}")
	public TodoModel updateTodo(@PathVariable(value = "id") long todoId, @Valid @RequestBody TodoModel newEditedTodo)
	{

		TodoModel todoModel = todoRepository.findById(todoId)
				.orElseThrow(() -> new ResourceNotFoundException("Todo", "id", todoId));

		todoModel.setSummary(newEditedTodo.getSummary());
		todoModel.setDescription(newEditedTodo.getDescription());

		TodoModel updatedTodo = todoRepository.save(todoModel);
		return updatedTodo;
	}

	// Delete a todo
	@DeleteMapping("/todo/{id}")
	public ResponseEntity<?> deleteTodo(@PathVariable(value = "id") long todoID)
	{
		TodoModel todoModel = todoRepository.findById(todoID)
				.orElseThrow(() -> new ResourceNotFoundException("Todo", "id", todoID));

		todoRepository.delete(todoModel);

		return ResponseEntity.ok().build();
	}
}