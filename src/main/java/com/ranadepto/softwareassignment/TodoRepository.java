/*
		Created by:Rana Depto

		Email:mail@ranadepto.com
	
	    Date:6/3/18,Time:4:24PM 
*/

package com.ranadepto.softwareassignment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoRepository extends JpaRepository<TodoModel, Long>
{

}