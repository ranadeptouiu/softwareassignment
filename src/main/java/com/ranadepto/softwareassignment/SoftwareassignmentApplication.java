package com.ranadepto.softwareassignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoftwareassignmentApplication
{
	public static void main(String[] args)
	{
		SpringApplication.run(SoftwareassignmentApplication.class, args);
	}
}
