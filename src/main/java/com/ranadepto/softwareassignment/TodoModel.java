/*
		Created by:Rana Depto

		Email:mail@ranadepto.com
	
	    Date:6/3/18,Time:4:24PM 
*/

package com.ranadepto.softwareassignment;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Entity
@Table(name = "Todo")

@EntityListeners(AuditingEntityListener.class)

public class TodoModel implements Serializable
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@NotBlank
	private String summary;

	@NotBlank
	private String description;

	public long getId()
	{
		return id;
	}

	public String getSummary()
	{
		return summary;
	}

	public String getDescription()
	{
		return description;
	}

	public void setId(long id)
	{

		this.id = id;
	}

	public void setSummary(String summary)
	{
		this.summary = summary;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}
}